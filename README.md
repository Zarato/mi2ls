# MI2LS (Math Image to LaTeX Sequence)
Implementation of the paper [Translating Math Formula Images to LaTeX Sequences Using Deep Neural Networks with Sequence-level Training](https://arxiv.org/abs/1908.11415) using [PyTorch](https://www.pytorch.org/).

## Table of contents
1. [About the project](#about-the-project)
    1. [MI2LS](#mi2ls)
    2. [Built with](#built-with)
2. [Getting started](#getting-started)
    1. [Prerequisites](#prerequisites)
    2. [Installation](#installation)
3. [Usage](#usage)
4. [License](#license)

<!-- ABOUT THE PROJECT -->
## About the project

### MI2LS
The paper [Translating Math Formula Images to LaTeX Sequences Using Deep Neural Networks with Sequence-level Training](https://arxiv.org/abs/1908.11415) proposed a new `seq2seq` model called `MI2LS` (Math Image To LaTeX Sequence). The model is based on an `Encoder`, a `decoder` and a `soft attention mechanism`.
![diagram](assets/model.jpg)

* Problem formulation

  The problem is formulated as a sequence prediction problem.
  Let $`(x,y) \in \R^{H \times W} \times \mathcal{V}^T`$ a pair of a grayscale image of size $`(H,W)`$ and the ground truth $`\LaTeX`$ sequence of size $`T`$. We already know the function $`f^{-1}: \mathcal{V}^T \to \R^{H \times W}`$ which is the $`\LaTeX`$ compiler. Our goal is to determine $`y`$ by knowing $`x`$, i.e., find the mapping function $`f: \R^{H \times W} \to \mathcal{V}^T`$.

  In order to find this mapping function $`f`$, we use supervised training on a dataset of size $`N`$ $`\{x_i, y_i\}_{1 \le i \le N}`$ in order to build a function $`\hat{f}`$ that approximates $`f`$.

* Encoder
  * Convolutional Neural Network
    Since its first introduction by `Yann LeCun` in the 1980s the CNN have been proven to be powerful in the field of computer vision. In this paper the author have chosen to use a CNN to extract features from the input images.

    A CNN is composed of convolution layers (an input image is convolved with a set of kernels), pooling layers (reduces the image size and increase the size of the receptive field), activation layers (nonlinearity to the neural network, in most CNN `ReLU` is used) and sometimes [batch normalization](https://arxiv.org/abs/1502.03167) which accelerates deep network.

    The authors decided to use a CNN architecture based on the [VGG-VeryDeep](https://arxiv.org/abs/1409.1556):

    | Type | Number of feature maps | Kernel size | Padding size | Stride size |
    | ---- | ---------------------- | ----------- | ------------ | ----------- |
    | BN | | | |
    | Convolution | 512 | (3, 3) | (1, 1) | (1, 1) |
    | MaxPooling | | (2, 1) | (0, 0) | (2 ,1) |
    | BN | | | |
    | Convolution | 512 | (3, 3) | (1, 1) | (1, 1) |
    | MaxPooling | | (1, 2) | (0, 0) | (1 ,2) |
    | Convolution | 256 | (3, 3) | (1, 1) | (1, 1) |
    | BN | | | |
    | Convolution | 256 | (3, 3) | (1, 1) | (1, 1) |
    | MaxPooling | | (2, 2) | (0, 0) | (2 ,2) |
    | Convolution | 128 | (3, 3) | (1, 1) | (1, 1) |
    | MaxPooling | | (2, 2) | (0, 0) | (2 ,2) |
    | Convolution | 64 | (3, 3) | (1, 1) | (1, 1) |
    | Input | Gray-scale image | | |

    At the end of the CNN, the dimension $`(H, W)`$ of the output feature maps are 8 times smaller $`(H', W')`$, and each position is $`D=512`$ dimensions deep.

    **Note**: In this project the [VGG](https://arxiv.org/abs/1409.1556) network is not the one mainly used. I also implemented different pretrained CNN such as:
    - [ResNet](https://arxiv.org/abs/1512.03385)
    - [Inception V3](https://arxiv.org/abs/1512.00567)
    - [MobileNet V3](https://arxiv.org/abs/1905.02244)
    
    So the ``input size`` and ``feature maps`` may vary.

  * Positional Encoding

    In math formulas the spatial relationship between the symbols extend in different directions: left-right, top-down, etc. The positional relationships between mathematical symbols carry an essential mathematical semantics. In order to preserve spatial locality, the authors decided to adapt the 1-D positional encoding technique proposed in the [Transformer model](https://arxiv.org/pdf/1706.03762.pdf) to 2-D as follows:

    ```math
    \operatorname{PE}(x, y, 2i) = \sin\left(\frac{x}{10000^{4i/d_{model}}}\right)\newline

    \operatorname{PE}(x, y, 2i + 1) = \cos\left(\frac{x}{10000^{4i/d_{model}}}\right)\newline
    
    \operatorname{PE}(x, y, 2i + \frac{D}{2}) = \sin\left(\frac{y}{10000^{4i/d_{model}}}\right)\newline

    \operatorname{PE}(x, y, 2i + 1 + \frac{D}{2}) = \cos\left(\frac{y}{10000^{4i/d_{model}}}\right)\newline
    ```

    where $`x,y`$ specifies the horizontal and vertical positions, and $`i,j \in \llbracket 0, \frac D 4 - 1 \rrbracket`$. These signals are added to the feature maps.

    After adding the signals to the feature maps, the result is unfolded into 1-D array $`\vec{E}=(e_1, e_2, \dots, e_L) \in  \left(\R^{1 \times D}\right)^L \approx \R^{L \times 1 \times D}`$ where $`L=H' \cdot W'`$ is the length of the array, $`\forall i \in \llbracket 1, L \rrbracket, e_i \in \R^{1 \times D} \approx \R^D`$ has a dimension of $`D`$ which is the feature size.

  * Decoder
  
    The task after encoding the input image into a feature maps is to decode it into a sequence of $`\LaTeX`$ tokens.
    For doing that, RNN are well suited because it "remembers" the previous prediction.
    The task of the RNN is to approximate the conditional probability:
    
    ```math
    \mathbb{P}(y_t|y_1, \dots, y_{t-1}, E)
    ```
    
    where $`y_t \in \mathcal{V}`$ is a token.
    
    For doing so we start by projecting the token $`y_t \in \mathcal{V}`$ into a high-dimensional real space.
    The authors proposed to use a ``word embedding`` layer widely used in NLP
    ```math
    w_t = \operatorname{embedding}(y_t)
    ```
    
    Then the next part of the decoder is the RNN which will predict the tokens. The paper proposed a [stacked bidirectional long-short term memory (LSTM)](https://arxiv.org/abs/2005.11627).
    ![bi-stacked-lstm](assets/bilstm.png)
    Stacking layers of RNN allows to capture more complex language semantics, and using bidirectional cells helps to capture the contexts from forward and backward directions.
    
    The choice of LSTM over other RNNs is justified by the fact that LSTM is more capable of handling long sequences unlike other RNNs which are subject to the vanishing gradient with the growth of sequence length.
    ![lstm-cell](assets/LSTM_Cell.svg)
    The LSTM cell is composed of 3 gates($`i_t`$ the input gate, $`f_t`$ the forget gate and $`o_t`$ the output gate) and 2 states($`h_t`$ the hidden state and $`c_t`$ the cell state).
    
    ```math
    f_t = \sigma(W_f \cdot x_t + U_f \cdot h_{t-1} + b_f)\newline
    
    i_t = \sigma(W_i \cdot x_t + U_i \cdot h_{t-1} + b_i)\newline
    
    o_t = \sigma(W_o \cdot x_t + U_o \cdot h_{t-1} + b_o)\newline
    
    c_t = f_t \odot c_{t-1} + i_t \odot \tanh(W_c \cdot x_t + U_c \cdot h_{t-1} + b_c)\newline
    
    h_t = o_t \odot c_t
    ```
    
    where $`\sigma : x \mapsto \frac{1}{1 + e^{-x}}`$ is the sigmoid function and $`\odot`$ is the [Hadamard product](https://en.wikipedia.org/wiki/Hadamard_product_(matrices)).
    
    The initial values are $`c_0=h_0=0`$.
    
    As we encode the input image with a CNN we don't have the initial hidden state and cell state of the decoder.
    
    Therefore we will train the initial states based on the encoder output:
    
    ```math
    h_0 = \tanh\left(W_h \cdot \left(\frac{1}{H' \cdot W'} \sum_{i=1}^{H' \cdot W'}{\overrightarrow{e_i}} \right) + b_h\right)\newline
    
    c_0 = \tanh\left(W_c \cdot \left(\frac{1}{H' \cdot W'} \sum_{i=1}^{H' \cdot W'}{\overrightarrow{e_i}} \right) + b_c\right)
    ```
    
  * Attention mechanism
    
    The sequence of $`\LaTeX`$ tokens can very large and unfortunately a RNN can not handle such a long sequence without losing information.
    
    The [attention mechanism](https://arxiv.org/abs/1706.03762) has been introduced to solve this problem and is now widely used in the domain of Computer Vision and NLP.
    
    For doing so we calculate a context vector $`C_t`$ based on the encoder output. Here we adopt a soft attention mechanism, i.e. we calculate the context vector as a linear combination of the encoder output.
    
    ```math
    \overrightarrow{C_t} = \sum_{i=1}^{H' \cdot W'}{\alpha_{it} \overrightarrow{e_i}}\newline
    
    a_{it} = \langle \overrightarrow{\beta}, \tanh(W_1 \cdot \overrightarrow{h_{t-1}} + W_2 \cdot \overrightarrow{e_i}) \rangle\newline
    
    \alpha_{it} = \operatorname{softmax}(a_{it})\newline
    
    \operatorname{softmax}(a_{it}) = \frac{\exp(a_{it})}{\sum_{k=1}^{H' \cdot W'}{\exp(a_{kt})}}
    ```
    
    where $`\alpha_{it}`$ is the ``i``-th attention weight at time ``t``. The weights indicate which parts of the encoder output we should be focused on.
    
    Then we have to propagate the context vector into the RNN and for doing so we introduce a an ``attentional hidden state vector``.
    
    ```math
    O_t = \tanh(W_3 \cdot [h_t, C_t])
    ```
    
    where $`[.,.]`$ the concatenation of two vectors: $`[.,.] : (x,y) \in \mathbb{R}^n \times \mathbb{R}^m \mapsto (x_1,\dots,x_n,y_1,\dots,y_m) \in \mathbb{R}^{n+m}`$.
    
    The context vector is then fed to the RNN as:
    
    ```math
    h_t = \operatorname{RNN}(h_{t-1}, [w_{t-1}, O_{t-1}])
    ```
    
    and the probability of the next token is computed as:
    
    ```math
    \mathbb{P}(y_t) = \operatorname{softmax}(W_4 \cdot O_t)
    ```
    
    For the initial ``attentional hidden state vector`` we adopt a similar approach as previously:
    ```math
    O_0 = \tanh\left(W_O \cdot \left(\frac{1}{H' \cdot W'} \sum_{i=1}^{H' \cdot W'}{\overrightarrow{e_i}} \right) + b_O\right)
    ```
    

### Built with

* [Python](https://www.python.org/)
* [PyTorch](https://www.pytorch.org/)
<!-- * [Docker](https://www.docker.com/) -->

<!-- GETTING STARTED -->
## Getting started
In order to properly use this application, you have to setup a python environment such as [Conda](https://docs.conda.io/en/latest/) or [venv](https://docs.python.org/3/library/venv.html).

### Prerequisites
* venv
  1. Setup the virtual environment
  ```sh
  python -m venv ./venv/
  ```
  2. Activate the virtual environment
    * Windows
    ```sh
    .\venv\Scripts\activate.bat # or activate.ps1 if you are using Powershell
    ```
    * Unix or MacOS:
    ```sh
    source ./venv/bin/activate
    ```
  3. Install the requirements
  ```sh
  pip install -r requirements.txt
  ```

* Conda
  * If you want to create a new `Conda` environment:
  ```sh
  conda env create -f environment.yml
  ```
  * If you want to update your current `Conda` environment:
  ```sh
  conda env update -f environment.yml
  ```

### Installation
TODO

## Usage

### Training
TODO

<!--
### Web application
TODO

### CLI
TODO

### GUI
TODO
-->

## Roadmap

- [ ] Model
  - [x] Encoder
    - [x] VGG
    - [x] Positional encoding
  - [ ] Decoder
    - [ ] Token embeddings
    - [ ] Stacked bidirectional LSTM
  - [ ] Attention
- [ ] Dataset
  - [ ] IM2LATEX-100K
  - [ ] StackExchange
  - [ ] Arxiv
- [ ] Training
- [ ] Web app
- [ ] GUI
- [ ] CLI

## License
Distributed under the MIT License. See `LICENSE` for more information.