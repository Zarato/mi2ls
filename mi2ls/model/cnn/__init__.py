"""This module contains some CNNs."""
from .resnet import Resnet, resnet18, resnet34, resnet50
from .vgg import VGG

import typing as ty
import torch
import torch.nn as nn
import torchvision


def _get_vgg(arch: str = 'vgg16_bn', **kwargs: ty.Any) -> nn.Module:
    r"""Create a VGG network according to the paper `Very Deep Convolutional Networks for Large-Scale Image
    Recognition <https://arxiv.org/abs/1409.1556>`.

    :param arch: Name of the VGG network
    :type arch: str
    :param kwargs: Additional arguments to be given to the network constructor
    :return: A VGG network
    :rtype: torch.nn.Module
    :raises ValueError: If ``arch`` is not a valid VGG network
    """
    models: ty.Dict[str, ty.Callable] = {
        'vgg11': torchvision.models.vgg11,
        'vgg13': torchvision.models.vgg13,
        'vgg16': torchvision.models.vgg16,
        'vgg19': torchvision.models.vgg19,
        'vgg11_bn': torchvision.models.vgg11_bn,
        'vgg13_bn': torchvision.models.vgg13_bn,
        'vgg16_bn': torchvision.models.vgg16_bn,
        'vgg19_bn': torchvision.models.vgg19_bn,
    }
    if arch not in models:
        raise ValueError(f"{arch} is not a valid VGG model.")

    return models[arch](**kwargs)


def _get_inception_v3(**kwargs: ty.Any) -> nn.Module:
    """Create an InceptionV3 (GoogleNetV3) network according to the paper `Rethinking the Inception Architecture for
    Computer Vision <https://arxiv.org/abs/1512.00567>`.

    :param kwargs: Additional arguments to be given to the network constructor
    :return: An InceptionV3 network
    :rtype: torch.nn.Module
    """
    kwargs['aux_logits'] = False
    model: nn.Module = torchvision.models.inception_v3(**kwargs)
    # get only the features
    return nn.Sequential(*(
        list(model.children())[:-3]
    ))


def _get_mobilenet_v3(
        arch: str = 'mobilenet_v3_large', **kwargs: ty.Any) -> nn.Module:
    """Create a MobileNetV3 network according to the paper `Searching for MobileNetV3
    <https://arxiv.org/abs/1905.02244>`.

    :param arch: Name of the MobileNetV3 network (``large`` or ``small``)
    :type arch: str
    :param kwargs: Additional arguments to be given to the network constructor
    :return: A MobileNetV3 network
    :rtype: torch.nn.Module
    :raises ValueError: If ``arch`` is not a valid MobileNetV3 network
    """
    models: ty.Dict[str, ty.Callable] = {
        'mobilenet_v3_large': torchvision.models.mobilenet_v3_large,
        'mobilenet_v3_small': torchvision.models.mobilenet_v3_small
    }
    if arch not in models:
        raise ValueError(f"{arch} is not a valid MobilenetV3 model.")

    model: nn.Module = models[arch](**kwargs)
    # return only the features
    return model.features


def _get_resnet(arch: str = 'resnet50', **kwargs: ty.Any) -> nn.Module:
    """Create a ResNet network according to the paper `Deep Residual Learning for Image Recognition
    <https://arxiv.org/abs/1512.03385>`.

    :param arch: Name of the ResNet network
    :type arch: str
    :param kwargs: Additional arguments to be given to the network constructor
    :return: A ResNet network
    :rtype: torch.nn.Module
    :raises ValueError: If ``arch`` is not a valid ResNet network
    """
    models: ty.Dict[str, ty.Callable] = {
        'resnet18': torchvision.models.resnet18,
        'resnet34': torchvision.models.resnet34,
        'resnet50': torchvision.models.resnet50,
        'resnet101': torchvision.models.resnet101,
        'resnet152': torchvision.models.resnet152,
        'resnext50_32x4d': torchvision.models.resnext50_32x4d,
        'resnext101_32x8d': torchvision.models.resnext101_32x8d,
        'wide_resnet50_2': torchvision.models.wide_resnet50_2,
        'wide_resnet101_2': torchvision.models.wide_resnet101_2,
    }
    if arch not in models:
        raise ValueError(f"{arch} is not a valid Resnet model.")

    model: nn.Module = models[arch](**kwargs)
    return nn.Sequential(*(
        list(model.children())[:-2]
    ))


def get_cnn(name: str, **kwargs: ty.Any) -> ty.Tuple[torch.nn.Module, ty.Tuple[int, int]]:
    """Create one of the following CNNs: `VGG`, `InceptionV3`, `MobileNetV3` and `ResNet`.

    :param name: Name of the CNN
    :type name: str
    :param kwargs: Additional arguments to be given to the network constructor
    :return: A CNN and the input size
    :rtype: (torch.nn.Module, (int, int))
    :raises ValueError: If ``arch`` is not a valid CNN
    """
    if 'mobilenet' in name:
        return _get_mobilenet_v3(name, **kwargs), (224, 224)
    elif 'resne' in name:
        return _get_resnet(name, **kwargs), (224, 224)
    elif 'inception' in name:
        return _get_inception_v3(**kwargs), (299, 299)
    elif 'vgg' in name:
        return _get_vgg(name, **kwargs), (224, 224)

    raise ValueError(f"{name} is not a valid CNN model.")


__all__ = [
    'Resnet',
    'resnet18',
    'resnet34',
    'resnet50',
    'VGG',
    'get_cnn'
]
