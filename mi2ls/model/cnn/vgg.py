"""This module contains the VGG network."""
import torch
import torch.nn as nn


class VGG(nn.Module):
    """Create a VGG-VeryDeep network.

    The CNN architecture is:

    +---------+------------------------+-------------+--------------+-------------+
    | Type    | Number of feature maps | Kernel size | Padding size | Stride size |
    +=========+========================+=============+==============+=============+
    | BN      | -                      | -           | -            | -           |
    +---------+------------------------+-------------+--------------+-------------+
    | Conv    | 512                    | (3, 3)      | (1, 1)       | (1, 1)      |
    +---------+------------------------+-------------+--------------+-------------+
    | MaxPool |                        | (2, 1)      | (0, 0)       | (2, 1)      |
    +---------+------------------------+-------------+--------------+-------------+
    | BN      | -                      | -           | -            | -           |
    +---------+------------------------+-------------+--------------+-------------+
    | Conv    | 512                    | (3, 3)      | (1, 1)       | (1, 1)      |
    +---------+------------------------+-------------+--------------+-------------+
    | MaxPool |                        | (1, 2)      | (0, 0)       | (1, 2)      |
    +---------+------------------------+-------------+--------------+-------------+
    | Conv    | 256                    | (3, 3)      | (1, 1)       | (1, 1)      |
    +---------+------------------------+-------------+--------------+-------------+
    | BN      | -                      | -           | -            | -           |
    +---------+------------------------+-------------+--------------+-------------+
    | Conv    | 256                    | (3, 3)      | (1, 1)       | (1, 1)      |
    +---------+------------------------+-------------+--------------+-------------+
    | MaxPool |                        | (2, 2)      | (0, 0)       | (2, 2)      |
    +---------+------------------------+-------------+--------------+-------------+
    | Conv    | 128                    | (3, 3)      | (1, 1)       | (1, 1)      |
    +---------+------------------------+-------------+--------------+-------------+
    | MaxPool |                        | (2, 2)      | (0, 0)       | (2, 2)      |
    +---------+------------------------+-------------+--------------+-------------+
    | Conv    | 64                     | (3, 3)      | (1, 1)       | (1, 1)      |
    +---------+------------------------+-------------+--------------+-------------+
    | Input   |                        |             |              |             |
    +---------+------------------------+-------------+--------------+-------------+

    """  # noqa

    _features: nn.Module

    def __init__(self):
        """Initialize the VGG network."""
        super().__init__()

        # input image size: height x width
        self._features = nn.Sequential(
            # since we work with a grayscale image the depth is 1
            nn.Conv2d(1, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),

            # first conv block
            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0,
                         dilation=1, ceil_mode=False),
            # placing ReLU after maxpooling makes the computation faster
            # because there are fewer elements
            nn.ReLU(inplace=True),

            # second conv block
            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0,
                         dilation=1, ceil_mode=False),
            nn.ReLU(inplace=True),

            # third conv block
            nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=3,
                      stride=1, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),

            # fourth conv block
            nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(
                kernel_size=(1, 2),
                padding=0, stride=(1, 2),
                dilation=1, ceil_mode=False),
            nn.ReLU(inplace=True),

            # fifth conv block
            nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(512, 512, kernel_size=3,
                      stride=1, padding=1, bias=False),
            nn.BatchNorm2d(512),
            nn.MaxPool2d(
                kernel_size=(2, 1),
                padding=0, stride=(2, 1),
                dilation=1, ceil_mode=False),
            nn.ReLU(inplace=True),

            # sixth conv block
            nn.Conv2d(512, 512, kernel_size=3,
                      padding=1, stride=1, bias=False),
            nn.BatchNorm2d(512)
        )

    def _init_weights(self, module: nn.Module) -> None:
        # all the layers are stored in `self.features`
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                # by default PyTorch initialize
                # Conv2D's weights with Kaiming Uniform
                nn.init.kaiming_normal_(
                    m.weight, mode='fan_out', nonlinearity='relu')
            # don't modify batch norm weights because
            # it is by default initialized to 1, and 0 for bias
            elif isinstance(m, nn.Sequential):
                self._init_weights(m)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass.

        :param x: Input data
        :type x: torch.Tensor
        :return: Output data
        :rtype: torch.Tensor

        .. topic:: Shape

            - Input: :math:`(H, W)` or :math:`(B, C, H, W)`
            - Output: :math:`(B, d_{model}, H', W')` where :math:`(H', W') = (H, W) // 8`

        .. note::
            In this case, :math:`d_{model} = 512`.
        """  # noqa
        if len(x.shape) == 2:
            # H x W
            x = x.repeat(1, 1, 1, 1)  # (B, C, H, W)
        elif len(x.shape) == 3:
            # B x H x W
            x = x.unsqueeze(1)  # (B, C, H, W)

        assert len(x.shape) == 4

        return self._features(x)
