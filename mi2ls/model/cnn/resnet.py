"""This module contains Resnet networks."""
import torch
import torch.nn as nn
import typing as ty
import abc


class BuildingBlock(nn.Module, abc.ABC):
    """The building block for resnet network."""

    expansion: int = 1
    _inplanes: int
    _planes: int
    _stride: int
    _downsample: ty.Optional[nn.Module]
    relu: nn.ReLU

    def __init__(
            self, inplanes: int, planes: int, stride: int = 1,
            downsample: ty.Optional[nn.Module] = None):
        """Initialize the building block.

        :param inplanes: The number of input planes
        :type inplanes: int
        :param planes: The number of planes
        :type planes: int
        :param stride: Stride, defaults to 1
        :type stride: int, optional
        :param downsample: Downsample, defaults to None
        :type downsample: ty.Optional[nn.Module], optional
        """
        super().__init__()
        self._inplanes = inplanes
        self._planes = planes
        self._stride = stride
        self._downsample = nn.Identity() if downsample is None else downsample
        self.relu = nn.ReLU(inplace=True)

    @abc.abstractmethod
    def encode(self, x: torch.Tensor) -> torch.Tensor:
        """Encode the input.

        :param x: Input data
        :type x: torch.Tensor
        :raises NotImplementedError:
        :return: Encoded input data
        :rtype: torch.Tensor
        """
        raise NotImplementedError()

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass.

        :param x: Input data
        :type x: torch.Tensor
        :return: Output data
        :rtype: torch.Tensor
        """
        out: torch.Tensor = self.encode(x)
        residual: torch.Tensor = self._downsample(x)

        out += residual
        out = self.relu(out)
        return out


class BasicBlock(BuildingBlock):
    """Basic building block for resnet networks. Used in resnet18 and resnet34."""  # noqa

    features: nn.Sequential
    expansion: int = 1

    def __init__(self, inplanes: int, planes: int, stride: int = 1,
                 downsample: ty.Optional[nn.Module] = None):
        """Initialize the building block.

        :param inplanes: The number of input planes
        :type inplanes: int
        :param planes: The number of planes
        :type planes: int
        :param stride: Stride, defaults to 1
        :type stride: int, optional
        :param downsample: Downsample, defaults to None
        :type downsample: ty.Optional[nn.Module], optional
        """
        super().__init__(inplanes, planes, stride, downsample)

        self.features = nn.Sequential(
            # layer 1
            nn.Conv2d(
                inplanes, planes, kernel_size=3, stride=stride, padding=1,
                bias=False),
            nn.BatchNorm2d(planes),
            self.relu,
            # layer 2
            nn.Conv2d(planes, planes, kernel_size=3,
                      stride=1, padding=1, bias=False),
            nn.BatchNorm2d(planes)
        )

    def encode(self, x: torch.Tensor) -> torch.Tensor:
        """Encode the input.

        :param x: Input data
        :type x: torch.Tensor
        :return: Output data
        :rtype: torch.Tensor
        """
        return self.features(x)


class Bottleneck(BuildingBlock):
    """Bottleneck building block for resnet networks. Used in resnet50 and above."""  # noqa

    features: nn.Sequential
    expansion: int = 4

    def __init__(self, inplanes: int, planes: int, stride: int = 1,
                 downsample: ty.Optional[nn.Module] = None):
        """Initialize the building block.

        :param inplanes: The number of input planes
        :type inplanes: int
        :param planes: The number of planes
        :type planes: int
        :param stride: Stride, defaults to 1
        :type stride: int, optional
        :param downsample: Downsample, defaults to None
        :type downsample: ty.Optional[nn.Module], optional
        """
        super().__init__(inplanes, planes, stride, downsample)

        self.features = nn.Sequential(
            # layer 1
            nn.Conv2d(
                inplanes, planes, kernel_size=1, stride=stride, bias=False),
            nn.BatchNorm2d(planes),
            self.relu,
            # layer 2
            nn.Conv2d(planes, planes, kernel_size=3,
                      stride=1, padding=1, bias=False),
            nn.BatchNorm2d(planes),
            self.relu,
            # layer 3
            nn.Conv2d(planes, planes * 4, kernel_size=1, bias=False),
            nn.BatchNorm2d(planes * 4)
        )

    def encode(self, x: torch.Tensor) -> torch.Tensor:
        """Encode the input.

        :param x: Input data
        :type x: torch.Tensor
        :return: Output data
        :rtype: torch.Tensor
        """
        return self.features(x)


class Resnet(nn.Module):
    """Resnet network implemented according to the paper `Deep Residual Learning for Image Recognition <https://arxiv.org/abs/1512.03385>`."""  # noqa

    inplanes: int
    features: nn.Module

    def __init__(
            self, block: ty.Type[BuildingBlock], layers: ty.List[int],
            depth: int = 3):
        """Initialize the resnet network.

        :param block: Type of building block
        :type block: ty.Type[BuildingBlock]
        :param layers: Number of layers
        :type layers: ty.List[int]
        :param depth: Input depth, defaults to 3
        :type depth: int, optional
        """
        assert len(layers) == 4, "Resnet is designed with 4 layers"
        super().__init__()

        # does not include fully connected layer
        self.inplanes = 64

        self.features = nn.Sequential(
            # input
            nn.Conv2d(depth, 64, kernel_size=7,
                      stride=2, padding=3, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2, padding=0, ceil_mode=True),

            # layer 1
            self._make_layer(block, 64, layers[0]),
            # layer 2
            self._make_layer(block, 128, layers[1], stride=2),
            # layer 3
            self._make_layer(block, 256, layers[2], stride=2),
            # layer 4
            self._make_layer(block, 512, layers[3], stride=2),
            # don't add avgpool2d because we need 512 feature maps
        )
        if self.training:
            self._init_weights(self.features)

    def _make_layer(
            self, block: ty.Type[BuildingBlock],
            planes: int, n_blocks: int, stride: int = 1) -> nn.Module:
        downsample: ty.Optional[nn.Module] = None

        # downsample is performed by conv3_1, conv4_1,
        # and conv5_1 with a stride of 2
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(
                    self.inplanes, planes * block.expansion, kernel_size=1,
                    stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion))

        layers: ty.List[nn.Module] = [
            block(self.inplanes, planes, stride, downsample),
            *
            [block(self.inplanes, planes * block.expansion)
             for _ in range(1, n_blocks)]]
        self.inplanes = planes * block.expansion

        return nn.Sequential(*layers)

    def _init_weights(self, module: nn.Module) -> None:
        # all the layers are stored in `self.features`
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                # by default PyTorch initialize
                # Conv2D's weights with Kaiming Uniform
                nn.init.kaiming_normal_(
                    m.weight, mode='fan_out', nonlinearity='relu')
            # don't modify batch norm weights because
            # it is by default initialized to 1, and 0 for bias
            elif isinstance(m, nn.Sequential):
                self._init_weights(m)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass.

        :param x: Input data
        :type x: torch.Tensor
        :return: Features map
        :rtype: torch.Tensor
        """
        return self.features(x)


def resnet18(**kwargs) -> Resnet:
    """Build a resnet18.

    :return: An instance of Resnet
    :rtype: Resnet
    """
    return Resnet(BasicBlock, [2, 2, 2, 2], **kwargs)


def resnet34(**kwargs) -> Resnet:
    """Build a resnet34.

    :return: An instance of Resnet
    :rtype: Resnet
    """
    return Resnet(BasicBlock, [3, 4, 6, 3], **kwargs)


def resnet50(**kwargs) -> Resnet:
    """Build a resnet50.

    :return: An instance of Resnet
    :rtype: Resnet
    """
    # Note : using Bottleneck makes 2048 features maps
    return Resnet(Bottleneck, [3, 4, 6, 3], **kwargs)
