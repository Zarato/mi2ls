import torch
import torch.nn as nn
import torch.nn.functional as F
import typing as ty
import mi2ls.utils


class Attention(nn.Module):
    """Attention mechanism according to the paper `Attention is all you need <https://arxiv.org/abs/1706.03762>`.
    """

    beta: nn.Parameter
    W1h: nn.Linear
    W2e: nn.Linear
    actv: ty.Callable

    def __init__(self, d_model: int, hidden_size: int = 512,
                 actv: ty.Callable = nn.Tanh()):
        """Create a soft attention mechanism.

        :param d_model: Number of features
        :type d_model: int
        :param hidden_size: Size of the hidden state vector
        :type hidden_size: int
        :param actv: Activation function
        :type actv: callable
        """

        assert mi2ls.utils.is_valid_activation(actv), "Not a valid activation function"
        super().__init__()

        self.beta = nn.Parameter(torch.rand(d_model), requires_grad=True)
        self.W1h = nn.Linear(in_features=hidden_size, out_features=d_model, bias=False)
        self.W2e = nn.Linear(in_features=d_model, out_features=d_model, bias=False)
        self.actv = actv

    def forward(self, ht: torch.Tensor, encoder_output: torch.Tensor) -> torch.Tensor:
        r"""Soft attention mechanism

        The context vector is calculated according to the following formula:

        .. math::
            \overrightarrow{C_t} = \sum_{i=1}^{H' \cdot W'}{\alpha_{it} \overrightarrow{e_i}}

        with :math:`\overrightarrow{C_t}, \overrightarrow{e_i} \in \mathbb{R}^D` the context vector at time ``t`` and
        the ``i``-th feature vector and :math:`\alpha_{it} \in \mathbb{R}` is the ``i``-th weight at time ``t``.

        The attention weights are calculated according to:

        .. math::
            a_{it} = \langle \overrightarrow{\beta}, \tanh(W_1 \cdot \overrightarrow{h_{t-1}} + W_2 \cdot \overrightarrow{e_i}) \rangle

            \alpha_{it} = \operatorname{softmax}(a_{it})

            \operatorname{softmax}(a_{it}) = \frac{\exp(a_{it})}{\sum_{k=1}^{H' \cdot W'}{\exp(a_{kt})}}

        :param ht: Current hidden state
        :type ht: torch.Tensor
        :param encoder_output: Encoder's output
        :type encoder_output: torch.Tensor
        :return: Context tensor
        :rtype: torch.Tensor

        .. topic:: Shape

            - Hidden state: :math:`(B, H_{out})`
            - Encoder output: :math:`(B, H' \cdot W', d_{model})`
        """
        energy: torch.Tensor = self.actv(self.W1h(ht) + self.W2e(encoder_output))  # [B, H*W, hout]
        energy = energy.transpose(1, 2)  # [B, Hout, H*W]
        beta: torch.Tensor = self.beta.repeat(encoder_output.size(0), 1).unsqueeze(1)  # [B, 1, Hout]

        a: torch.Tensor = torch.bmm(beta, energy)  # [B, 1, H*W]
        a = a.squeeze(1)  # [B, H*W]
        alpha: torch.Tensor = F.softmax(a, dim=1)  # [B, H*W]

        context: torch.Tensor = torch.bmm(alpha.unsqueeze(1), encoder_output)  # [B, 1, D]
        context = context.squeeze(1)  # [B, D]

        return context
