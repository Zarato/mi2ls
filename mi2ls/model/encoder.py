"""This module contains the Encoder needed for the model."""
import torch
import torch.nn as nn


class Encoder(nn.Module):
    """Encode the input images into abstract feature representations.

    It is composed of a CNN and positional encoding.
    """

    _cnn: nn.Module
    _positional_encoding: nn.Module

    def __init__(self, cnn: nn.Module, encoding: nn.Module):
        """Initialize the encoder.

        :param cnn: A CNN to extract features from the input image
        :type cnn: torch.nn.Module
        :param encoding: A positional encoding for richer representation of \
            spatial locality information
        :type encoding: torch.nn.Module
        """
        super(Encoder, self).__init__()

        self._cnn = cnn
        self._positional_encoding = encoding

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass.

        :param x: Input data
        :type x: torch.Tensor
        :return: Output data
        :rtype: torch.Tensor

        .. topic:: Shape

            - Input: :math:`(H, W)` or :math:`(B, C, H, W)`
            - Output: :math:`(B, d_{model}, H', W')`
        """  # noqa
        # extract features from the input images
        out: torch.Tensor = self._cnn(x)
        # positional encoding
        out = self._positional_encoding(out)
        return out
