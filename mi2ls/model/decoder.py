import torch
import torch.nn as nn
import typing as ty
import torch.nn.functional as F

RNN = ty.Union[ty.Type[nn.RNN], ty.Type[nn.LSTM], ty.Type[nn.GRU]]


class Decoder(nn.Module):
    """Decode the feature representation into token representation.

    It is composed of a RNN
    """
    _num_states: int
    _hidden_size: int
    _rnn: nn.RNNBase
    _init_h: nn.Linear
    _init_c: nn.Linear
    _init_o: nn.Linear
    _dropout: nn.Dropout

    def __init__(self, d_model: int, emb_size: int, hidden_size: int, num_layers: int = 2, bidirectional: bool = True,
                 rnn: RNN = nn.LSTM, dropout: float = 0.):
        """Initialize the decoder

        :param d_model: Number of features
        :type d_model: int
        :param emb_size: Size of each embedding vector
        :type emb_size: int
        :param hidden_size: Hidden size
        :type hidden_size: int
        :param num_layers: Number of layers in the RNN, defaults to 2
        :type num_layers: int
        :param bidirectional: Use bidirectional, defaults to True
        :type bidirectional: bool
        :param rnn: RNN architecture, defaults to torch.nn.LSTM
        :type rnn: type(torch.nn.RNNBase)
        :param dropout: Probability of dropping out, defaults to 0.
        :type dropout: float
        """
        super(Decoder, self).__init__()

        self._num_states: int = (1 + int(bidirectional)) * num_layers
        self._hidden_size = hidden_size
        self._rnn = rnn(emb_size + hidden_size, hidden_size, num_layers=num_layers, batch_first=True,
                        bidirectional=bidirectional)

        self._init_h = nn.Linear(d_model, self._num_states * hidden_size)  # this is the same as creating
        # `num_states` nn.Linear
        self._init_c = nn.Linear(d_model, self._num_states * hidden_size)
        self._init_o = nn.Linear(d_model, hidden_size)

        self._dropout = nn.Dropout(p=dropout)

    def forward(self, dec_states: ty.Tuple[torch.Tensor, torch.Tensor], o_t: torch.Tensor, prev_w: torch.Tensor) -> \
            ty.Tuple[torch.Tensor, torch.Tensor]:
        r"""Forward pass.

        :param dec_states: RNN hidden states
        :type dec_states: tuple(torch.Tensor, torch.Tensor)
        :param o_t: Attentional hidden state vector
        :type o_t: torch.Tensor
        :param prev_w: Previous word embedding
        :type prev_w: torch.Tensor
        :return: Hidden states
        :rtype: tuple(torch.Tensor, torch.Tensor)

        .. topic:: Shape

            - States: :math:`(D * {num_{layers}}, B, H_{out})`
            - Attentional hidden state vector: :math:`(B, H_{out})`
            - Previous word embedding: :math:`(B, {emb_{size}})`
        """
        input_rnn: torch.Tensor = torch.cat([prev_w, o_t], dim=1)  # (B, emb_size + h_out)
        h_t, c_t = self._rnn(input_rnn, dec_states) # (B, H_out) * 2
        h_t = self._dropout(h_t)
        c_t = self._dropout(h_t)

        return h_t, c_t

    def init_decoder(self, enc_out: torch.Tensor) -> ty.Tuple[ty.Tuple[torch.Tensor, torch.Tensor], torch.Tensor]:
        r"""Initialize the initial states based on the encoder output.

        :param enc_out: Encoder output
        :type enc_out: torch.Tensor
        :return: Hidden states and initial attention
        :rtype: tuple(tuple(torch.Tensor, torch.Tensor), torch.Tensor)

        .. topic:: Shape

            - Input: :math:`(B, H' \cdot W', d_{model})`
            - Hidden states: :math:`(D * {num_{layers}}, B, H_{out})`
            - Initial attention: :math:`(B, H_{out})`
            where :math:`D=2` if `bidirectional` else 1
        """
        mean: torch.Tensor = enc_out.mean(dim=1)  # (B, d_model)

        def init_state(initializer: nn.Linear) -> torch.Tensor:
            state: torch.Tensor = F.tanh(initializer(mean))  # (B, n_states * hidden_size)
            state = state.reshape(-1, self._num_states, self._hidden_size)  # (B, n_states, hidden_size)
            state = state.permute(1, 0, 2)  # (n_states, B, hidden_size)
            return state

        h: torch.Tensor = init_state(self._init_h)
        c: torch.Tensor = init_state(self._init_c)
        o: torch.Tensor = F.tanh(self._init_o(mean))  # (B, hidden_size)

        return (h, c), o
