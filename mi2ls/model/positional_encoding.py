"""This module contains the 1D and 2D positional encoding."""
import torch
import torch.nn as nn
import math


def make_pe_1d(d_model: int, max_len: int) -> torch.Tensor:
    r"""Create a 1D positional encoding described in `Attention is all you need <https://arxiv.org/pdf/1706.03762.pdf>`.

    1D Positional encodings are defined as:

    .. math::
        \operatorname{PE}(x, 2i) = \sin\left(\frac{x}{10000^{2i/d_{model}}}\right)

        \operatorname{PE}(x, 2i + 1) = \cos\left(\frac{x}{10000^{2i/d_{model}}}\right)


    :param d_model: Dimension of inputs
    :type d_model: int
    :param max_len: Maximum length of positional encodings
    :type max_len: int
    :return: Positional encodings
    :rtype: torch.Tensor

    .. topic:: Shape

        - Output: :math:`(max_{len}, 1, d_{model})
    """  # noqa
    pe: torch.Tensor = torch.zeros(max_len, d_model)

    position: torch.Tensor = torch.arange(
        0, max_len, dtype=torch.float).unsqueeze(1)
    div_term: torch.Tensor = torch.exp(
        torch.arange(0, d_model, 2, dtype=torch.float) *
        (-math.log(10000.) / d_model))
    terms: torch.Tensor = position * div_term

    pe[:, 0::2] = torch.sin(terms)
    pe[:, 1::2] = torch.cos(terms)
    pe = pe.unsqueeze(1)
    return pe


def make_pe_2d(d_model: int, max_h: int, max_w: int) -> torch.Tensor:
    r"""Create a 2D positional encoding described in `Attention is all you need <https://arxiv.org/pdf/1706.03762.pdf>`.

    2D Positional encodings are defined as:

    .. math::
        \operatorname{PE}(x, y, 2i) = \sin\left(\frac{x}{10000^{4i/d_{model}}}\right)

        \operatorname{PE}(x, y, 2i + 1) = \cos\left(\frac{x}{10000^{4i/d_{model}}}\right)

        \operatorname{PE}(x, y, 2i + \frac{D}{2}) = \sin\left(\frac{y}{10000^{4i/d_{model}}}\right)

        \operatorname{PE}(x, y, 2i + 1 + \frac{D}{2}) = \cos\left(\frac{y}{10000^{4i/d_{model}}}\right)


    :param d_model: Dimension of inputs
    :type d_model: int
    :param max_h: Maximum height of positional encodings
    :type max_h: int
    :param max_w: Maximum width of positional encodings
    :return: Positional encodings
    :rtype: torch.Tensor

    .. topic:: Shape

        - Output: :math:`(d_{model}, max_h, max_w)
    """  # noqa
    pe_h: torch.Tensor = torch.zeros(max_h, d_model // 2)
    pe_w: torch.Tensor = torch.zeros(max_w, d_model // 2)

    position_h: torch.Tensor = torch.arange(
        0, max_h, dtype=torch.float).unsqueeze(1)
    position_w: torch.Tensor = torch.arange(
        0, max_w, dtype=torch.float).unsqueeze(1)

    div_term: torch.Tensor = torch.exp(
        torch.arange(0, d_model // 2, 2, dtype=torch.float) *
        (-math.log(10000.) / (d_model // 2)))

    terms_h: torch.Tensor = position_h * div_term
    terms_w: torch.Tensor = position_w * div_term

    pe_h[:, 0::2] = torch.sin(terms_h)
    pe_h[:, 1::2] = torch.cos(terms_h)
    pe_h = pe_h.unsqueeze(1).permute(2, 0, 1).expand(-1, -1, max_w)

    pe_w[:, 0::2] = torch.sin(terms_w)
    pe_w[:, 1::2] = torch.cos(terms_w)
    pe_w = pe_w.unsqueeze(1).permute(2, 1, 0).expand(-1, max_h, -1)

    return torch.cat([pe_h, pe_w], dim=0)


class PositionalEncoding1D(nn.Module):
    """1D positional encoding implemented according to the paper `Attention is all you need <https://arxiv.org/pdf/1706.03762.pdf>`."""  # noqa

    pe: torch.Tensor
    dropout: nn.Dropout

    def __init__(
            self, d_model: int, max_len: int = 5000):
        """Initialize the 1D positional encoding.

        :param d_model: Input dimension
        :type d_model: int
        :param max_len: Maximum length of positional encodings, defaults to 5000
        :type max_len: int, optional
        """  # noqa
        super().__init__()
        self.register_buffer("pe", make_pe_1d(d_model, max_len))

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass.

        :param x: Input data
        :type x: torch.Tensor
        :return: Output data
        :rtype: torch.Tensor
        """
        assert x.shape[2] == self.pe.shape[2]

        x = x + self.pe[:, x.size(0)]
        return x


class PositionalEncoding2D(nn.Module):
    """2D positional encoding implemented according to the paper `Attention is all you need <https://arxiv.org/pdf/1706.03762.pdf>`."""  # noqa

    pe: torch.Tensor

    def __init__(self, d_model: int, max_h: int = 2000, max_w: int = 2000):
        """Initialize the 1D positional encoding.

        :param d_model: Input dimension, must be even
        :type d_model: int
        :param max_h: Maximum height, defaults to 2000
        :type max_h: int, optional
        :param max_w: Maximum weight, defaults to 2000
        :type max_w: int, optional
        """
        assert d_model % 2 == 0, f"Dimension must be even, actual: {d_model}"

        super().__init__()
        self.register_buffer("pe", make_pe_2d(d_model, max_h, max_w))

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass.

        :param x: Input data
        :type x: torch.Tensor
        :return: Output data
        :rtype: torch.Tensor

        .. topic:: Shape

            - Input: :math:`(B, d_{model}, H, W)`
            - Output: :math:`(B, d_{model}, H, W)`
        """
        assert x.shape[1] == self.pe.shape[0]

        x = x + self.pe[:, :x.size(2), :x.size(3)]
        return x
