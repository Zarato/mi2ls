"""This module contains the different parts of the model, as well as the model itself."""  # noqa
from .encoder import Encoder
from .positional_encoding import PositionalEncoding1D, PositionalEncoding2D
from .cnn import Resnet, resnet18, resnet34, resnet50, VGG, get_cnn

__all__ = [
    'Resnet',
    'resnet18',
    'resnet34',
    'resnet50',
    'VGG',
    'Encoder',
    'PositionalEncoding1D',
    'PositionalEncoding2D',
    'get_cnn'
]
