import inspect
import typing as ty
import torch.nn as nn


def is_valid_activation(actv: ty.Callable, n_parameters: int = 1) -> bool:
    """Check if a given function is a valid activation function by checking its number of parameters.

    :param actv: Function
    :type actv: callable
    :param n_parameters: Number of parameters required, defaults 1
    :type n_parameters: int
    :return: True if the number of parameters matches
    :rtype: bool
    """
    if isinstance(actv, nn.Module):
        actv = actv.forward

    return len(inspect.signature(actv).parameters) == n_parameters
