"""Implementation of the paper \
`Translating Math Formula Images to LaTeX Sequences Using Deep Neural Networks with Sequence-level Training <https://arxiv.org/abs/1908.11415>`."""  # noqa
from .version import __version__

__all__ = [
    '__version__'
]
